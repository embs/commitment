from rest_framework import serializers

from commitment.models import Commit, Repo


class RepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Repo
        fields = ('id', 'name')


class CommitSerializer(serializers.ModelSerializer):
    repo = RepoSerializer()

    class Meta:
        model = Commit
        fields = ('id', 'message', 'author', 'authored_at', 'repo')


class ReadOnlySerializer(serializers.Serializer):
    def create(self, _):
        pass

    def update(self, _a, _b):
        pass


class PaginatorSerializer(ReadOnlySerializer):
    count = serializers.CharField()
    num_pages = serializers.CharField()

    class Meta:
        fields = ('count', 'num_pages')


class CommitPageSerializer(ReadOnlySerializer):
    number = serializers.CharField()
    paginator = PaginatorSerializer()
    object_list = CommitSerializer(many=True)

    class Meta:
        fields = ('number', 'paginator', 'object_list')
