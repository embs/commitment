import datetime

from github import Github

from commitment import transports
from commitment.exceptions import RepoAlreadyAdded
from commitment.models import Commit, Repo

from users.models import User


def get_or_create_github_user(code):
    user_info = transports.get_user_info(code)
    email = user_info['email']
    access_token = user_info['access_token']

    try:
        return User.objects.get(email=email)
    except User.DoesNotExist:
        user = User(email=email, access_token=access_token)

    user.save()

    return user


def create_github_webhook(repo):
    config = {'url': 'https://cmmtmnt.herokuapp.com/ghhook', 'content_type': 'json'}

    return repo.create_hook('web', config, ['push'], active=True)


def import_commits(user, repo_name):
    g = Github(user.access_token)
    now = datetime.datetime.now()
    a_month_ago = now - datetime.timedelta(days=30)
    repo = g.get_repo(repo_name)

    if Repo.objects.filter(github_id=repo.id).exists():
        existing_repo = Repo.objects.filter(github_id=repo.id).first()
        if existing_repo.users.filter(id=user.id).exists():
            raise RepoAlreadyAdded()
        else:
            existing_repo.users.add(user)

            return existing_repo

    new_repo = Repo(github_id=repo.id, name=repo.full_name)
    new_repo.save()
    new_repo.users.add(user)
    create_github_webhook(repo)
    for commit in repo.get_commits(since=a_month_ago):
        author = commit.commit.author
        new_commit = Commit(sha=commit.sha, message=commit.commit.message,
                            authored_at=commit.commit.author.date, repo=new_repo,
                            author="{} <{}>".format(author.name, author.email))
        new_commit.save()

    return new_repo
