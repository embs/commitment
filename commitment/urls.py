from django.conf import settings
from django.conf.urls import include, url  # noqa
from django.urls import path
from django.contrib import admin
from django.views.generic import TemplateView

import django_js_reverse.views

from commitment import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^jsreverse/$', django_js_reverse.views.urls_js, name='js_reverse'),

    url(r'^$', TemplateView.as_view(template_name='root.html',
        extra_context={"client_id": settings.CLIENT_ID}), name='root'),

    path('ghauthcallback', views.github_authentication_callback, name='ghauthcallback'),
    path('ghhook', views.github_webhook, name='ghhook'),
    path('app', views.app, name='app'),
    path('signout', views.signout, name='signout'),
    path('commits', views.commits, name='commits'),
    path('repos', views.repos, name='repos'),
    path('accounts/login/', TemplateView.as_view(template_name='root.html'), name='root'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
