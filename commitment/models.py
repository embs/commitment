from django.db import models

from common.models import IndexedTimeStampedModel

from users.models import User


class Repo(IndexedTimeStampedModel):
    github_id = models.CharField(default='', max_length=255)
    name = models.CharField(default='', max_length=255)
    users = models.ManyToManyField(User)


class Commit(IndexedTimeStampedModel):
    sha = models.CharField(default='', max_length=255)
    message = models.TextField(default='')
    author = models.CharField(default='', max_length=255)
    authored_at = models.DateTimeField(default=None)
    repo = models.ForeignKey(Repo, default=1, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-authored_at']
