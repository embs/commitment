class RepoAlreadyAdded(Exception):
    """Thrown when repo was already added."""
