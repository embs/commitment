import imp

from django.contrib.auth import decorators
from django.http import HttpResponseRedirect

from unittest import TestCase
from unittest.mock import patch, MagicMock

from commitment import views

class FakeRequest:
    def __init__(self, GET):
        self.GET = GET


class GitHubAuthenticationCallbackWithCodeTestCase(TestCase):
    def setUp(self):
        self.code = '123'
        self.fake_request = FakeRequest({'code': self.code})


class GitHubAuthenticationCallbackSuccessTestCase(
        GitHubAuthenticationCallbackWithCodeTestCase):
    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.login')
    def test_fetches_or_creates_github_user_with_proper_args(self, _,
            get_or_create_github_user_mock):
        views.github_authentication_callback(self.fake_request)

        get_or_create_github_user_mock.assert_called_with(self.code)

    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.login')
    def test_signs_user_in(self, login_mock, get_or_create_github_user_mock):
        user_from_github = MagicMock()
        get_or_create_github_user_mock.return_value = user_from_github

        views.github_authentication_callback(self.fake_request)

        login_mock.assert_called_with(self.fake_request, user_from_github)


    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.login')
    def test_returns_redirect_to_proper_url(self, _a, _b):
        result = views.github_authentication_callback(self.fake_request)

        self.assertIs(type(result), HttpResponseRedirect)
        self.assertEqual(result.url, 'app')


class GitHubAuthenticationCallbackErrorTestCase(
        GitHubAuthenticationCallbackWithCodeTestCase):
    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.messages')
    def test_returns_redirect_to_proper_url(self, _, get_or_create_github_user_mock):
        get_or_create_github_user_mock.side_effect = RuntimeError()

        result = views.github_authentication_callback(self.fake_request)

        self.assertIs(type(result), HttpResponseRedirect)
        self.assertEqual(result.url, '/')

    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.messages')
    def test_sets_proper_message(self, messages_mock, get_or_create_github_user_mock):
        get_or_create_github_user_mock.side_effect = RuntimeError()

        views.github_authentication_callback(self.fake_request)

        messages_mock.error.assert_called_with(self.fake_request,
                'It was not possible to access your GitHub user.')


class GitHubAuthenticationCallbackWithoutCodeTestCase(TestCase):
    def setUp(self):
        self.fake_request = FakeRequest({})


    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.messages')
    def test_returns_redirect_to_proper_url(self, _, get_or_create_github_user_mock):
        get_or_create_github_user_mock.side_effect = RuntimeError()

        result = views.github_authentication_callback(self.fake_request)

        self.assertIs(type(result), HttpResponseRedirect)
        self.assertEqual(result.url, '/')

    @patch('commitment.views.get_or_create_github_user')
    @patch('commitment.views.messages')
    def test_sets_proper_message(self, messages_mock, get_or_create_github_user_mock):
        get_or_create_github_user_mock.side_effect = RuntimeError()

        views.github_authentication_callback(self.fake_request)

        messages_mock.error.assert_called_with(self.fake_request,
                'It was not possible to access your GitHub user.')

class AppTestCase(TestCase):
    def setUp(self):
        self.code = '123'
        self.fake_request = FakeRequest(self.code)
        decorators.login_required = lambda x: x
        imp.reload(views)

    @patch('commitment.views.render')
    def test_render_app(self, render_mock):
        views.app(self.fake_request)
        render_mock.assert_called_with(self.fake_request, 'app.html')


class SignoutTestCase(TestCase):
    def setUp(self):
        self.fake_request = FakeRequest({})

    @patch('commitment.views.logout')
    def test_logout(self, logout_mock):
        views.signout(self.fake_request)
        logout_mock.assert_called_with(self.fake_request)

    @patch('commitment.views.logout')
    def test_returns_redirect_to_proper_url(self, _a):
        result = views.signout(self.fake_request)

        self.assertIs(type(result), HttpResponseRedirect)
        self.assertEqual(result.url, '/')
