from unittest import TestCase
from unittest.mock import patch, MagicMock

from commitment import transports


@patch('commitment.transports.settings')
@patch('commitment.transports.get_access_token')
@patch('commitment.transports.requests')
class GetUserInfoTestCase(TestCase):
    def test_calls_get_access_token(self, _, get_access_token_mock, settings_mock):
        client_id = MagicMock()
        client_secret = MagicMock()
        code = MagicMock()
        settings_mock.CLIENT_ID = client_id
        settings_mock.CLIENT_SECRET = client_secret

        transports.get_user_info(code)

        get_access_token_mock.assert_called_with(client_id, client_secret, code)

    def test_requests_url(self, requests_mock, get_access_token_mock, _):
        access_token = MagicMock()
        get_access_token_mock.return_value = access_token
        url = "https://api.github.com/user?access_token={token}".format(token=access_token)

        transports.get_user_info(MagicMock())

        requests_mock.get.assert_called_with(url)

    def test_returns_json_response(self, requests_mock, get_access_token_mock, _b):
        response = MagicMock()
        access_token = MagicMock()
        requests_mock.get.return_value = response
        response.json.return_value = {}
        get_access_token_mock.return_value = access_token

        result = transports.get_user_info(MagicMock())

        self.assertEqual(result, {'access_token': access_token})


@patch('commitment.transports.requests')
class GetAccessTokenTestCase(TestCase):
    def test_returns_access_token(self, requests_mock):
        token = '123'
        github_response = MagicMock()
        github_response.status_code = 200
        github_response.text = 'access_token={token}'.format(token=token)
        requests_mock.post.return_value = github_response

        result = transports.get_access_token(1, 2, 3)

        self.assertEqual(result, token)


@patch('commitment.transports.requests')
class GetAccessTokenErrorTestCase(TestCase):
    def test_returns_access_token(self, requests_mock):
        github_response = MagicMock()
        github_response.status_code = 500
        requests_mock.post.return_value = github_response

        self.assertRaises(Exception, transports.get_access_token, 1, 2, 3)
