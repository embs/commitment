from unittest import TestCase
from unittest.mock import patch

from commitment import services


class GetOrCreateGitHubUserWhenUserExists(TestCase):
    @patch('commitment.services.transports')
    @patch('commitment.services.User')
    def test_calls_transports(self, _, transports_mock):
        code = '123'

        services.get_or_create_github_user(code)

        transports_mock.get_user_info.assert_called_with(code)

    @patch('commitment.services.transports')
    @patch('commitment.services.User')
    def test_get_user_from_db(self, user_class_mock, transports_mock):
        code = '123'
        email = 'user@example.com'
        transports_mock.get_user_info.return_value = {'email': email}

        services.get_or_create_github_user(code)

        user_class_mock.objects.get.assert_called_with(email=email)


class GetOrCreateGitHubUserWhenUserDoesNotExist(TestCase):
    def setUp(self):
        user_patcher = patch('commitment.services.User')
        transports_patcher = patch('commitment.services.transports')
        self.patchers = [user_patcher, transports_patcher]
        self.user_class_mock = user_patcher.start()
        self.transports_mock = transports_patcher.start()
        self.new_user_instance = self.user_class_mock.return_value
        self.user_class_mock.DoesNotExist = Exception
        self.user_class_mock.objects.get.side_effect = self.user_class_mock.DoesNotExist()
        self.code = '123'
        self.email = 'user@example.com'
        self.transports_mock.get_user_info.return_value = {'email': self.email}

        self.result = services.get_or_create_github_user(self.code)

    def tearDown(self):
        for patcher in self.patchers:
            patcher.stop()

    def test_calls_transports(self):
        self.transports_mock.get_user_info.assert_called_with(self.code)

    def test_instantiates_user(self):
        self.user_class_mock.assert_called_with(email=self.email)

    def test_saves_user(self):
        self.new_user_instance.save.assert_called()

    def test_returns_user(self):
        self.assertEqual(self.result, self.new_user_instance)
