import json

from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from github import UnknownObjectException

from rest_framework.renderers import JSONRenderer

from commitment.exceptions import RepoAlreadyAdded
from commitment.models import Commit, Repo
from commitment.serializers import CommitPageSerializer
from commitment.services import get_or_create_github_user, import_commits


def github_authentication_callback(request):
    try:
        user = get_or_create_github_user(request.GET['code'])
    except (RuntimeError, KeyError):
        messages.error(request, "It was not possible to access your GitHub user.")

        return HttpResponseRedirect('/')

    login(request, user)

    return HttpResponseRedirect('app')


@login_required
def app(request):
    print(request.user)
    return render(request, 'app.html')


@login_required
def signout(request):
    logout(request)

    return HttpResponseRedirect('/')


@login_required
def repos(request):
    if request.method == 'GET':
        user_repos = Repo.objects.filter(users=request.user)
        response = serializers.serialize("json", user_repos)
        return HttpResponse(response)
    if request.method == 'POST':
        params = json.loads(request.body.decode('utf-8'))
        try:
            repo = import_commits(request.user, params['name'])
            body = serializers.serialize('json', [repo])
            return HttpResponse(body)
        except UnknownObjectException:
            return JsonResponse({'error': 'not found'}, status=422)
        except RepoAlreadyAdded:
            return JsonResponse({'error': 'already exists'}, status=422)
        else:
            return None
    return None


@login_required
def commits(request):
    repo_filter = request.GET.get('repo')
    user_repos = Repo.objects.filter(users=request.user)
    if repo_filter:
        user_repos = user_repos.filter(name=repo_filter)
    user_commits = Commit.objects.filter(repo__in=user_repos)
    page = request.GET.get('page')
    paginator = Paginator(user_commits, 5)
    user_commits = paginator.get_page(page)
    serializer = CommitPageSerializer(user_commits)

    return HttpResponse(JSONRenderer().render(serializer.data))


@csrf_exempt
def github_webhook(request):
    params = json.loads(request.body.decode('utf-8'))
    repo = Repo.objects.filter(name=params['repository']['full_name']).first()
    for commit in params['commits']:
        author = commit['author']
        new_commit = Commit(sha=commit['id'], message=commit['message'],
                            authored_at=commit['timestamp'], repo=repo,
                            author="{} <{}>".format(author['name'],
                                                    author['email']))
        new_commit.save()

    return HttpResponse(status=201)
