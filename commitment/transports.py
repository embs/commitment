from django.conf import settings
import json
import requests


def get_user_info(code):
    access_token = get_access_token(settings.CLIENT_ID, settings.CLIENT_SECRET, code)
    url = "https://api.github.com/user?access_token={token}".format(token=access_token)
    user_info = requests.get(url).json()
    user_info['access_token'] = access_token

    return user_info


def get_access_token(client_id, secret, code):
    headers = {'Content-Type': 'application/json'}
    body = {'client_id': client_id, 'client_secret': secret, 'code': code}
    res = requests.post('https://github.com/login/oauth/access_token',
                        data=json.dumps(body), headers=headers)

    if res.status_code != 200:
        raise Exception("Github authentication error")

    res_params = {k: v for k, v in
                  [p.split('=') for p in res.text.split('&')]}

    return res_params['access_token']
