### commmitment

### Setup

Copy `settings/local.py`:

    $ cp commitment/settings/local.py.example commitment/settings/local.py

Copy `.env`:

    $ cp .env.example .env

Migrate database:

    $ ./manage.py migrate

Install Django app dependencies:

    $ pipenv install --dev

Use pipenv environment (where Django dependencies are available):

    # needed for running stuff depending on pip packages (e.g., ./manage.py)
    $ pipenv shell

Install node app dependencies:

    $ npm install

### CLI Tools

Django shell

    $ ./manage.py shell

DB shell

    $ ./manage.py dbshell

### Run

Start node app:

    $ npm start

Start Django app:

    $ ./manage.py runserver

Start Celery:

    $ ./manage.py celery

### Test

    $ ./manage.py test

Specific test case(s):

    $ ./manage.py test commitment.tests.test_views

### Check code coverage

    $ bin/coverage

### Verify style

    $ prospector

### Verify build

    $ bin/ci
