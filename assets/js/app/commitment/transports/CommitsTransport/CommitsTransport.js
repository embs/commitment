import axios from 'axios';

class CommitsTransport {
  all(page, callback) {
    axios.get(`/commits?page=${page}`)
         .then(response => {
           callback(response);
         })
         .catch(error => {
           callback(error.response);
         });
  }

  forRepo(repo, page, callback) {
    axios.get(`/commits?repo=${repo}&page=${page}`)
         .then(response => {
           callback(response);
         })
         .catch(error => {
           callback(error.response);
         });
  }
}

export default CommitsTransport;
