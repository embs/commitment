import axios from 'axios';

class ReposTransport {
  constructor() {
    this.cookie = document.cookie
                          .split(';')
                          .map(ck => {
                            const tokens = ck.split('=');
                            return { name: tokens[0], value: tokens[1] };
                          })
                          .find(ck => {
                            return ck.name == 'csrftoken'
                          });
  }

  all(callback) {
    axios.get('/repos')
         .then(response => {
           callback(response);
         })
         .catch(error => {
           callback(error.response);
         });
  }

  post(repo, callback) {
    axios.post('/repos',
      {
        name: repo
      },
      {
        headers: { 'X-CSRFToken': this.cookie.value, 'Content-Type': 'application/json' }
      })
         .then(response => {
           callback(response);
         })
         .catch(error => {
           callback(error.response);
         });
  }
}

export default ReposTransport;
