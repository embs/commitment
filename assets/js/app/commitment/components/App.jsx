import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader';

import Commits from './Commits';
import Navbar from './Navbar';
import Paginator from './Paginator';
import RepoInput from './RepoInput';
import Repos from './Repos';
import { CommitsTransport, ReposTransport } from '../transports';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.reposTransport = new ReposTransport();
    this.commitsTransport = new CommitsTransport();

    this.state = {
      commits: [],
      repos: [],
      currentPage: 0,
      totalPages: 0,
      selectedRepo: 'All repositories'
    };
  }

  addRepo(repo) {
    const repos = this.state.repos.slice(0);

    repos.push(repo[0]);

    this.setState({ repos });
    this.fetchCommits();
  }

  selectRepo(repo, page = 1) {
    this.setState({ selectedRepo: repo });

    if(repo === 'All repositories') {
      this.fetchCommits(page);
      return;
    }

    this.commitsTransport.forRepo(repo, page, r => this.updateCommitsList(r));
  }

  fetchCommits(page = 1) {
    this.commitsTransport.all(page, r => this.updateCommitsList(r));
  }

  updateCommitsList(response) {
    const { object_list, number, paginator } = response.data;

    this.setState({
      commits: object_list,
      currentPage: number,
      totalPages: paginator.num_pages
    });
  }

  componentDidMount() {
    this.reposTransport.all((response) => {
      this.setState({ repos: response.data });
    });
    this.fetchCommits();
  }

  changePage(page) {
    const { selectedRepo } = this.state;

    this.selectRepo(selectedRepo, page);
  }

  render() {
    const { commits, currentPage, repos, selectedRepo, totalPages } = this.state;

    return (
	  <div>
        <Navbar
          repos={repos}
          addRepo={repo => { this.addRepo(repo) }}
          selectRepo={repo => { this.selectRepo(repo) }}
          selectedRepo={selectedRepo}
        />
        <Commits
          commits={commits}
          selectRepo={repo => { this.selectRepo(repo) }}
        />
        <Paginator
          changePage={page => { this.changePage(page) }}
          currentPage={currentPage}
          totalPages={totalPages}
          totalCommits={commits.length}
        />
      </div>
    );
  }
}

export default hot(module)(App);
