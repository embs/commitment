import React from 'react';
import { hot } from 'react-hot-loader';

class Repos extends React.Component {
  renderNames() {
    const { repos } = this.props;

    return (
      repos.map(repo => (<li className="list-group-item" key={repo.pk}>{repo.fields.name}</li>))
    );
  }

  render() {
    return (
      <li>
        <h3>Tracked Repos</h3>
        <ul className="list-group">
          {this.renderNames()}
        </ul>
      </li>
    );
  }
}

export default hot(module)(Repos);
