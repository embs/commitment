import React from 'react';
import { hot } from 'react-hot-loader';


class Paginator extends React.Component {
  renderPageOptions() {
    const { changePage, currentPage, totalPages } = this.props;

    return [...Array(parseInt(totalPages)).keys()].map(pageNumber => {
      const number = pageNumber + 1;
      let active = '';

      if (parseInt(number) === parseInt(currentPage)) {
        active = 'active';
      }

      return (
        <li
          key={pageNumber}
          className={`page-item ${active}`}
        >
          <a
            className="page-link"
            href="#"
            onClick={e => { changePage(e.target.text) }}
          >
            {number}
          </a>
        </li>
      );
    });
  }

  render() {
    const { totalCommits } = this.props;

    if (totalCommits === 0) {
      return null;
    }

	return (
      <div className="d-flex justify-content-center">
        <div className="align-self-center">
          <nav className="mt-3" aria-label="Page navigation example">
            <ul className="pagination">
              {this.renderPageOptions()}
            </ul>
          </nav>
        </div>
      </div>
	)
  }
}

export default hot(module)(Paginator);
