import React from 'react';
import { hot } from 'react-hot-loader';
import Octicon, { Repo, SignOut } from '@githubprimer/octicons-react';

import Commits from './Commits';
import RepoInput from './RepoInput';
import Repos from './Repos';
import { ReposTransport } from '../transports';

class Navbar extends React.Component {
  renderRepositoryDropdownOptions() {
    const { repos } = this.props;

    if (repos.length === 0) {
      return <a className="dropdown-item disabled" href="#">You don't have any repositories.</a>
    }

    return repos.map(repo => (
      this.renderRepositoryDropdownOption(repo.fields.name, repo.pk)
    ));
  }

  renderRepositoryDropdownOption(text, key=null) {
    return (
      <a
        key={key}
        className="dropdown-item"
        href="#"
        onClick={e => { this.handleClickRepo(e) }}
      >
        {text}
      </a>
    );
  }

  handleClickRepo(event) {
    const { selectRepo } = this.props;
    const repo = event.target.text;

    event.preventDefault();
    selectRepo(repo);
  }

  render() {
    const { addRepo, selectedRepo } = this.props;

    return(
      <nav className="navbar navbar-expand-lg navbar-light bg-secondary">
        <a className="navbar-brand text-white" href="#">Commitment</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="/signout">
                <Octicon icon={SignOut} className="text-white" />
                <span className="ml-2 text-white">Sign Out</span>
              </a>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <Octicon icon={Repo} className="text-white" />
                <span className="ml-2 text-white">{selectedRepo}</span>
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                {this.renderRepositoryDropdownOption('All repositories')}
                {this.renderRepositoryDropdownOptions()}
              </div>
            </li>
          </ul>
          <RepoInput addRepo={repo => { addRepo(repo) }} />
        </div>
      </nav>
    )
  }
}

export default hot(module)(Navbar);
