import React from 'react';
import { shallow } from 'enzyme';

import RepoInput from '../RepoInput';


describe('RepoInput', () => {
  const wrapper = shallow((
    <RepoInput />
  ));

  function submitValue(value) {
    const input = wrapper.find('input');
	input.simulate('change', { target: value });
	input.simulate('keydown', { key: 'Enter' });
  }

  describe('with value which does not include a bar', () => {
    beforeEach(() => {
      submitValue('qwe');
    });

    it('sets error in state', () => {
      expect(wrapper.state('error')).toBe('Is this a "user/repo"?');
    });
  });

  describe('with value too short', () => {
    beforeEach(() => {
      submitValue('/');
    });

    it('sets error in state', () => {
      expect(wrapper.state('error')).toBe('Is this a "user/repo"?');
    });
  });

  describe('with valid value', () => {
    beforeEach(() => {
      submitValue('user/repo');
    });

    it('calls repos transport', () => {
    });
  });
});
