import React from 'react';
import { hot } from 'react-hot-loader';
import Octicon, { Plus } from '@githubprimer/octicons-react';

import { ReposTransport } from '../../transports';

class RepoInput extends React.Component {
  constructor(props) {
    super(props);

    this.reposTransport = new ReposTransport();

    this.state = {
      value: '',
      error: '',
      syncing: false
    };
  }

  submit() {
    if (this.valid()) {
      const { value } = this.state;
      const { addRepo } = this.props;

      this.setState({ syncing: true });

      this.reposTransport.post(value,
        (response) => {
          if (response.status === 200) {
            this.setState({ value: '', error: '' });
            addRepo(response.data);
          }
          else if (response.status === 422) {
            this.setState({ error: response.data.error });
          }
          this.setState({ syncing: false });
        }
      );
    } else {
      const error = 'Is this a "user/repo"?'

      this.setState({ error });
    }
  }

  valid() {
    const { value } = this.state;

    if (value.includes('/') && value.length >= 3) {
      return true;
    }

    return false;
  }

  inputErrorClass() {
    const { error } = this.state;

    if (error) {
      return 'is-invalid';
    }

    return '';
  }

  handleInputChange(event) {
    const value = event.target.value;

    this.setState({ value });
  }

  handleInputKeyDown(event) {
    this.setState({ error: '' });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.submit();
  }

  renderAddRepoButtonIcon() {
    const { syncing } = this.state;

    if (syncing) {
      return (
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        />
      )
    }
    else {
      return <Octicon icon={Plus} className="text-white" />;
    }
  }

  renderAddRepoButton() {
    const { syncing } = this.state;

    return (
      <button
        className={`btn btn-success ${syncing && 'disabled'} my-2 my-sm-0`}
        type="submit"
      >
        {this.renderAddRepoButtonIcon()}
        <span className="ml-2">Add repository</span>
      </button>
    )
  }

  render() {
    const helperTextId = 'repo-input-helper-text';
    const { value, error } = this.state;

    return (
      <form className="form-inline my-2 my-lg-0" onSubmit={(e) => this.handleSubmit(e)}>
        <span className="mr-sm-2 text-warning">
          {error}
        </span>

        <input
          className={`form-control mr-sm-2 ${this.inputErrorClass()}`}
          placeholder='username/repository-name'
          onChange={e => this.handleInputChange(e)}
          onKeyDown={e => this.handleInputKeyDown(e)}
          aria-describedby={helperTextId}
          value={value}
          aria-label="Add repository"
        />
        {this.renderAddRepoButton()}
      </form>
    );
  }
}

export default hot(module)(RepoInput);
