import React from 'react';
import { hot } from 'react-hot-loader';
import Octicon, { Calendar, GitCommit, Person, Repo } from '@githubprimer/octicons-react';
import moment from 'moment';

class Commits extends React.Component {
  renderCommits() {
    const { commits, selectRepo } = this.props;

    return commits.map(commit => {
      const lines = commit.message.split('\n');
      const summary = lines.shift();
      const body = lines.join('\n');

      return (
        <li key={commit.id} className="list-group-item list-group-item-light">
          <h5 className="mt-0 mb-1">
            <Octicon icon={GitCommit} className="mr-1" />
            {summary}
          </h5>
          <div>
            <Octicon icon={Calendar} className="mr-1" />
            <span className="mr-2">
              {moment(commit.authored_at).format('MMMM Do YYYY, h:mm:ss a')}
            </span>
            <Octicon icon={Person} className="mr-1" />
            <span className="mr-2">{commit.author}</span>
            <Octicon icon={Repo} className="mr-1" />
            <a
              href="#"
              onClick={e => selectRepo(e.target.text)}
            >
              {commit.repo.name}
            </a>
          </div>
          {body}
        </li>
      )
    });
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="commits__commit align-self-center">
          <ul className="list-group list-group-flush">
            {this.renderCommits()}
          </ul>
        </div>
      </div>
    );
  }
}

export default hot(module)(Commits);
