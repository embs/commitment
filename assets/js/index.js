import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap-includes';
import App from './app/commitment';
import '../sass/style.scss';

$('body').show();

var app = document.getElementById('app');
if (app.length !== 0) {
  ReactDOM.render(<App />, app);
}
